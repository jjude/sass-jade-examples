### Overview
This project contains webpage designs that I create as I learn SASS & JADE


### SimpleProductPage
Extremely simple single-page product site. On mobile, everything is stacked. On tablet & more, there is two column. Footer is single column at all breakpoints. JADE is not used.

Uses [compass][1], [susy-grid][2] & [sassy-buttons][3]

### License
You are free to do whatever you want to do with this code. If you find it useful, a tweet (@jjude) will be appreciated.

[1]: http://compass-style.org/
[2]: http://susy.oddbird.net/
[3]: http://jaredhardy.com/sassy-buttons/
